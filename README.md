# README #

ET: phone home

### About ###
A service that tracks the external IP address of a computer. The service receives 'pings' (REST calls) from a client (curl, httpie, etc.) to track IP addresses of the client. The primary use case for the service is to track your external IP - e.g. imagine you have an External IP which changes over time and you'd like to keep track of it or know what it currently is.

### Stack ###
The application stack is as follows:
- uvicorn: lightning-fast ASGI server
- FastAPI: high performance async capable API framework
- MongoDB: no-sql database

### Resources ###
This application is meant to be lightweight. Many factors can influence the performance of an application and the following info is meant to be a guideline for application resource usage - PLEASE DO YOUR OWN PERFORMANCE/LOAD testing to verify your specific needs.

The following data shows application resource usage (with recommendations in parenthesis):

- low: <=100 concurrent users
    - CPU: 267 mCPU (500 mCPU)
    - Memory: 84m (128m)
    - uvicorn workers: 1
- moderate: 100-800 concurrent users
    - CPU: 1256 mCPU (1500 mCPU)
    - Memory: 85m (128m)
    - uvicorn workers: 1
- high: >=1000 concurrent users
    - CPU: 1500 mCPU (2000 mCPU)
    - Memory: 95m (160m)
    - uvicorn workers: 1

Note that you can increase uvicorn workers which increases memory and may decrease cpu slightly; however, our experience is that the average response time goes up and you get less throughput. Instead of increasing workers we recommend increasing the number of instances of the application.

### Run ###
This application has two implementations; standalone and load balanced. The following sections provide more detail on each implementation but it should be noted that the load balancer serves as a true load balancer only (it is not needed for static file serving).

#### Standalone ####
Standalone run mode provides a simple run footprint, yielding a single instance which is acceptable for development, test or non-ha/load balanced production environments. Depending on your use case this can run with as little as 64mb of memory and 100m CPU. It should be noted that this implementation is also suitable if a load balancer is provided externally.

#### Load Balanced ####
Load Balanced run mode provides a HA (highly available) and/or load balanced implementation. A lighttpd web server serves as a reverse-proxy.
