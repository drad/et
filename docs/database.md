# Database

You must create the db and user inside of mongo prior to using the application. To do this:

- connect to your mongodb instance
- create the database: `use et`
- create the user:
```
db.createUser(
  {
    user: "mongo",
    pwd: "mongo",
    roles: [ { role: "readWrite", db: "et" } ]
  }
)
```
