# Examples

NOTICE: there's not a lot of need for examples in V2 as FastAPI generates documentation/example access which you can view for the app at `/docs/` of your deployment (note that `/docs/` is relative to your `$API_BASE_PATH/{config.api.versions["current"]}` setting, you can see the relative path on startup of the `API` service (with LOG_LEVEL set to DEBUG).
