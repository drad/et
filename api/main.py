#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2020 drad <drader@adercon.com>

import logging
import graypy
from fastapi import FastAPI

from core.routes import core_router
from pings.routes import pings_router
from users.routes import users_router
from config import config

api_path = f"{config.API_BASE_PATH}/{config.API_VERSION}"

logger_base = logging.getLogger("default")
logger_base.setLevel(logging.getLevelName(config.APP_LOGLEVEL))
graylog_handler = graypy.GELFUDPHandler(
    host=config.GRAYLOG_HOST, port=config.GRAYLOG_PORT
)
console_handler = logging.StreamHandler()
if "graylog" in config.LOG_TO:
    logger_base.addHandler(graylog_handler)
    # pass
# ~ if "console" in config.LOG_TO:
# ~ logger_base.addHandler(console_handler)
# pass

logger = logging.LoggerAdapter(
    logging.getLogger("default"),
    {"application_name": config.CORE["name"], "application_env": config.DEPLOY_ENV},
)

if config.FASTAPI["debug"]:
    logger.info("### App Started With Debug On ###")

logger.info(
    f"{config.CORE['name']} - v.{config.CORE['version']} ({config.CORE['modified']}) - {config.APP_LOGLEVEL} - {config.LOG_TO}"
)
logger.debug(f"- DB URI:     {config._DB_URI}")
logger.debug(f"- Base Path:  {api_path}")

app = FastAPI(
    title=f"{config.CORE['name']}",
    description=f"{config.CORE['description']}",
    version=f"{config.CORE['version']}",
    openapi_url=f"{api_path}/openapi.json",
    docs_url=f"{api_path}/docs",
    redoc_url=None,
)

app.include_router(
    pings_router,
    prefix=f"{api_path}/pings",
    tags=["pings"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    users_router,
    prefix=f"{api_path}/users",
    tags=["users"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    core_router,
    prefix=f"{config.API_BASE_PATH}",
    tags=["core"],
    responses={404: {"description": "Not found"}},
)


@app.on_event("startup")
async def app_startup():
    """
    Do tasks related to app initialization.
    """
    pass


@app.on_event("shutdown")
async def app_shutdown():
    """
    Do tasks related to app termination.
    """
    # This finishes the DB driver connection.
    config.close_db_client()
