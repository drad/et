#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2020 drad <drader@adercon.com>

from motor.motor_asyncio import AsyncIOMotorClient
from os import getenv
import toml


def load_config() -> dict:
    """ load config data from toml config file """
    conf = {}
    try:
        conf = toml.load("config/config.toml")
    except FileNotFoundError as e:
        print(f"File not found: {e}")
    except Exception as e:
        print(f"Other Exception: {e}")
    return conf


def close_db_client():
    DB_CLIENT.close()


BASE = load_config()
# top-level/special components of config (for quicker access)
FASTAPI = BASE["fastapi"]
API = BASE["api"]
CORE = BASE["core"]
DATABASE = BASE["database"]

API_BASE_PATH = getenv("API_BASE_PATH", "/api")
API_VERSION = API["versions"]["current"]
API_ADMIN_ACCESS_KEY = getenv(
    "API_ADMIN_ACCESS_KEY", "7I3zjQQ0CFgRq794uN6fGJQks9ez63mHY"
)

# ENV Based Variables
DEPLOY_ENV = getenv("DEPLOY_ENV", "prd")

# database
_DB_NAME = getenv("DB_NAME", "et")
_DB_URI = getenv("DB_URI", "mongodb://mongo:mongo@mongo:27017")

# logging
LOG_TO = getenv("LOG_TO", "console").split(",")
APP_LOGLEVEL = getenv("LOG_LEVEL", "DEBUG")
GRAYLOG_HOST = getenv("GRAYLOG_HOST", None)
GRAYLOG_PORT = int(getenv("GRAYLOG_PORT", "12201"))

# DB_CLIENT = AsyncIOMotorClient(**_get_client_config())
DB_CLIENT = AsyncIOMotorClient(_DB_URI)
DB = DB_CLIENT[_DB_NAME]
