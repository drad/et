#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2020 drad <drader@adercon.com>

from pydantic import BaseModel
from typing import Optional
from datetime import datetime


class PingBase(BaseModel):
    """Used to abstract out basic fields\n
    Arguments:
        BaseModel {[type]} -- [description]
    """

    user_id: str = None
    address: Optional[str] = None


class PingExt(PingBase):
    """Non-User supplied data (added by backend logic)"""

    created: datetime = None


class Ping(PingExt):
    """Actual model used at DB level\n
    Extends:
        UserBase
    Adds `id_` field.
    Variables:
        id_: str {[ObjectId]} -- [id at DB]
    """

    id_: str


class AdminReportResponse(BaseModel):
    """Response model for AdminReport"""

    user_id: str = None
    user_name: str = None
    count: int = None
