#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2020 drad <drader@adercon.com>

import logging

from bson.objectid import ObjectId
from config.config import DB, FASTAPI, API_ADMIN_ACCESS_KEY
from fastapi import APIRouter, Depends, HTTPException, Request
from typing import List
from starlette.status import HTTP_201_CREATED
from datetime import datetime

from .models import PingBase, PingExt, Ping, AdminReportResponse

logger = logging.getLogger("default")

pings_router = APIRouter()


def validate_object_id(id_: str):
    try:
        _id = ObjectId(id_)
    except Exception:
        if FASTAPI["debug"]:
            logger.warning("Invalid Object ID")
        raise HTTPException(status_code=400)
    return _id


async def _get_or_404(id_: str):
    _id = validate_object_id(id_)
    obj = await DB.ping.find_one({"_id": _id})
    if obj:
        return fix_id(obj)
    else:
        raise HTTPException(status_code=404, detail="Not found")


def fix_id(obj):
    if obj.get("_id", False):
        obj["id_"] = str(obj["_id"])
        return obj
    else:
        raise ValueError(f"No `_id` found! Unable to fix ID for: {obj}")


async def valid_user_id(user_id):
    return True if await DB.user.find_one({"user_id": user_id}) else None


@pings_router.get("/", response_model=List[Ping])
async def get_all(admin_access_key: str, limit: int = 10, skip: int = 0):
    """Endpoint to retrieve all Pings"""
    if admin_access_key and admin_access_key == API_ADMIN_ACCESS_KEY:
        model_cursor = DB.ping.find().skip(skip).limit(limit)
        r = await model_cursor.to_list(length=limit)
        return list(map(fix_id, r))
    else:
        raise HTTPException(status_code=401, detail="Invalid Access Key")


@pings_router.get("/admin-report/", response_model=List[AdminReportResponse])
async def admin_report(
    admin_access_key: str,
    start: datetime,
    end: datetime,
    limit: int = 10,
    skip: int = 0,
):
    """Count of pings per client over a given timespan\n
         - start: 'YYYY-MM-DDTHH:mm:ss.eee-00:00' (e.g. 2020-05-11T13:00:00.000-00:00)\n
         - end: 'YYYY-MM-DD HH:mm:ss.eee-00:00' (e.g. 2020-05-11T14:00:00.000-00:00)
    """
    r = None
    j = []
    if admin_access_key and admin_access_key == API_ADMIN_ACCESS_KEY:
        cursor = DB.ping.aggregate(
            [
                {"$match": {"created": {"$gte": start, "$lt": end}}},
                {"$group": {"_id": "$user_id", "count": {"$sum": 1}}},
                {
                    "$lookup": {
                        "from": "user",
                        "localField": "_id",
                        "foreignField": "user_id",
                        "as": "user",
                    }
                },
                {"$unwind": "$user"},
                {"$project": {"_id": 1, "count": 1, "user.name": 1}},
            ]
        )
        r = await cursor.to_list(length=limit)
        for c in r:
            ar = AdminReportResponse()
            ar.user_id = c["_id"]
            ar.user_name = c["user"]["name"]
            ar.count = c["count"]
            j.append(ar)
        return j
    else:
        raise HTTPException(status_code=401, detail="Invalid Access Key")


@pings_router.get("/history-report/", response_model=List[Ping])
async def history_report(user_id: str, limit: int = 10, skip: int = 0):
    """Shows the history of pings for a given user"""
    cursor = DB.ping.find().sort("created", -1).skip(skip).limit(limit)
    r = await cursor.to_list(length=limit)
    return list(map(fix_id, r))


@pings_router.get("/last", response_model=Ping)
async def get_last(user_id: str):
    """Endpoint to retrieve last ping"""
    cursor = DB.ping.find({"user_id": user_id}).sort("_id", -1).limit(1)
    r = await cursor.to_list(length=1)
    return fix_id(r[0]) if r else None


@pings_router.post("/", response_model=Ping, status_code=HTTP_201_CREATED)
async def add(obj: PingBase, request: Request):
    """Endpoint to add a new Ping\n
        NOTE: if `address` is not supplied the client.host (IP) from the request will be used"""
    o = PingExt()
    o.created = datetime.now()
    o.user_id = obj.user_id
    # if no address we assume use of request.client.host (client ip)
    if not obj.address:
        # get client's ip from request.
        # o.address = request.client.host
        # use x-real-ip or x-forwarded-for in k8s behind a lb and under an ingress
        o.address = request.headers["x-real-ip"]
    else:
        o.address = obj.address

    # check user_id to ensure it is valid
    if await valid_user_id(o.user_id):
        obj_op = await DB.ping.insert_one(o.dict())
        if obj_op.inserted_id:
            r = await _get_or_404(obj_op.inserted_id)
            r["id_"] = str(r["_id"])
            return r
    else:
        raise HTTPException(status_code=404, detail="User ID Not found")


@pings_router.get("/{id_}", response_model=Ping)
async def get_by_id(id_: ObjectId = Depends(validate_object_id)):
    """Endpoint to retrieve a specific item"""
    obj = await DB.ping.find_one({"_id": id_})
    if obj:
        obj["id_"] = str(obj["_id"])
        return obj
    else:
        raise HTTPException(status_code=404, detail="Not found")


@pings_router.delete("/{id_}", dependencies=[Depends(_get_or_404)], response_model=dict)
async def delete_by_id(id_: str):
    """Endpoint to delete a specific item"""
    obj_op = await DB.ping.delete_one({"_id": ObjectId(id_)})
    if obj_op.deleted_count:
        return {"status": f"deleted count: {obj_op.deleted_count}"}


@pings_router.put(
    "/{id_}",
    dependencies=[Depends(validate_object_id), Depends(_get_or_404)],
    response_model=Ping,
)
async def update(id_: str, obj_data: PingBase):
    """Endpoint to update a specific item with some or all fields"""
    obj_op = await DB.ping.update_one({"_id": ObjectId(id_)}, {"$set": obj_data.dict()})
    if obj_op.modified_count:
        return await _get_or_404(id_)
    else:
        raise HTTPException(status_code=304)
