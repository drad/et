#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2020 drad <drader@adercon.com>

from pydantic import BaseModel


class UserBase(BaseModel):
    """Used to abstract out basic fields\n
    Arguments:
        BaseModel {[type]} -- [description]
    """

    active: bool = True
    name: str = None


class UserExt(UserBase):
    """Non-User supplied data (added by backend logic)"""

    user_id: str = None


class User(UserExt):
    """Actual model used at DB level\n
    Extends:
        User
    Adds `id_` field.
    Variables:
        id_: str {[ObjectId]} -- [id at DB]
    """

    id_: str
