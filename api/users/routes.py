#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2020 drad <drader@adercon.com>

import logging
from uuid import uuid4

from bson.objectid import ObjectId
from config.config import DB, FASTAPI, API_ADMIN_ACCESS_KEY
from fastapi import APIRouter, Depends, HTTPException
from typing import List
from starlette.status import HTTP_201_CREATED

from .models import UserBase, UserExt, User

logger = logging.getLogger("default")

users_router = APIRouter()


def validate_object_id(id_: str):
    try:
        _id = ObjectId(id_)
    except Exception:
        if FASTAPI["debug"]:
            logger.warning("Invalid Object ID")
        raise HTTPException(status_code=400)
    return _id


async def _get_or_404(id_: str):
    _id = validate_object_id(id_)
    obj = await DB.user.find_one({"_id": _id})
    if obj:
        return fix_id(obj)
    else:
        raise HTTPException(status_code=404, detail="Not found")


def fix_id(obj):
    if obj.get("_id", False):
        obj["id_"] = str(obj["_id"])
        return obj
    else:
        raise ValueError(f"No `_id` found! Unable to fix ID for: {obj}")


@users_router.get("/", response_model=List[User])
async def get_all(admin_access_key: str, limit: int = 10, skip: int = 0):
    """Endpoint to retrieve all"""
    if admin_access_key and admin_access_key == API_ADMIN_ACCESS_KEY:
        model_cursor = DB.user.find().skip(skip).limit(limit)
        r = await model_cursor.to_list(length=limit)
        return list(map(fix_id, r))
    else:
        raise HTTPException(status_code=401, detail="Invalid Access Key")


@users_router.post("/", response_model=User, status_code=HTTP_201_CREATED)
async def add(obj: UserBase):
    """Endpoint to add a new User"""
    o = UserExt()
    o.user_id = str(uuid4())
    o.active = obj.active
    o.name = obj.name

    r = await DB.user.insert_one(o.dict())
    if r.inserted_id:
        obj = await _get_or_404(r.inserted_id)
        obj["id_"] = str(obj["_id"])
        return obj


@users_router.get("/{id_}", response_model=User)
async def get_by_id(id_: ObjectId = Depends(validate_object_id)):
    """Endpoint to retrieve a specific item"""
    obj = await DB.user.find_one({"_id": id_})
    if obj:
        obj["id_"] = str(obj["_id"])
        return obj
    else:
        raise HTTPException(status_code=404, detail="Not found")


@users_router.delete("/{id_}", dependencies=[Depends(_get_or_404)], response_model=dict)
async def delete_by_id(id_: str):
    """Endpoint to delete a specific item"""
    obj_op = await DB.user.delete_one({"_id": ObjectId(id_)})
    if obj_op.deleted_count:
        return {"status": f"deleted count: {obj_op.deleted_count}"}


@users_router.put(
    "/{id_}",
    dependencies=[Depends(validate_object_id), Depends(_get_or_404)],
    response_model=User,
)
async def update(id_: str, obj_data: UserBase):
    """Endpoint to update a specific item with some or all fields"""
    obj_op = await DB.user.update_one({"_id": ObjectId(id_)}, {"$set": obj_data.dict()})
    if obj_op.modified_count:
        return await _get_or_404(id_)
    else:
        raise HTTPException(status_code=304)
