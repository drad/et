#!/bin/sh

chmod a+w /dev/stderr
chmod a+w /dev/stdout

# 2020-03-18:drad: tmp disabling during development/testing
#echo "- starting rsyslogd daemon..."
#rsyslogd

echo "starting lighttpd..."
exec lighttpd -D -f /etc/lighttpd/lighttpd.conf

#echo "just keep container running..."
#tail -f /dev/null
