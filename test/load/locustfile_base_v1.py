#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2020 drad <drader@adercon.com>

# install: `pip install locustio`
# start server: `locust -f locustfile_base_v1.py`
# run/monitor test: http://localhost:8089/
#
# NOTE:
#   - weights [@task(4), @task(2)] - this means @task(4) will be executed twice as many times as @task(2)

import random
from locust import HttpLocust, TaskSet, task
from locust.wait_time import between
from user_data import v1_users

class WebsiteTasksBasic(TaskSet):
    def get_user(self):
        user = random.choice(v1_users)
        return user

    # get last: `http -j GET ${server}:${port}${base_path}/ping/last/${client_id}`
    @task(2)
    def get_last_ping(self):
        user = self.get_user()
        #print(f"- get last user: {user['id']}")
        r = self.client.get(f"/et/api/v1/ping/last/{user['id']}")
        #print(f"  - get last response: status: {r.status_code} - text: {r.text}")

    # create: `http -j POST ${server}:${port}${base_path}/ping/create client_id=${client_id}`
    @task(6)
    def add_auto_ping(self):
        user = self.get_user()
        #print(f"- posting (auto) with user={user['id']}")
        r = self.client.post(f"/et/api/v1/ping/create", json={"client_id": user['id']})
        #print(f"  - post response: status: {r.status_code} - text: {r.text}")

    # create: `http -j POST ${server}:${port}${base_path}/ping/create client_id=${client_id} address="$(curl -s ifconfig.co)"`
    @task(3)
    def add_manual_ping(self):
        user = self.get_user()
        #print(f"- posting (manual) with user={user['id']}")
        r = self.client.post(f"/et/api/v1/ping/create", json={"client_id": user['id'], "address": "10.33.1.2"})
        #print(f"  - post response: status: {r.status_code} - text: {r.text}")


class WebsiteUser(HttpLocust):
    task_set = WebsiteTasksBasic
    host = "http://localhost:8008"
    # ~ host = "https://dradux.com"

    wait_time = between(5, 15)   # between 5 and 15 seconds
