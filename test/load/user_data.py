#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2020 drad <drader@adercon.com>

#
#   DEVELOPMENT ENVIRONMENT
#
# ~ v1_users = [
# ~ {'id': '52a880e3-fa90-4f35-bb55-d5fcec18dbb3', 'access_key': '93c3dc59-10ba-4700-a164-206a00f4859e'},   # MA House #1
# ~ {'id': '792bce11-d584-437c-bab6-01447e909100', 'access_key': '337435fd-79bf-490a-b670-af91d3a19327'},   # MA House #2
# ~ {'id': '26734ec7-b7b6-499b-bd86-0c89bf7eb738', 'access_key': 'e495e898-0821-435f-856f-dac2b818d7ce'},   # MA House #3
# ~ {'id': '0c19f105-a59e-43c5-9168-fa48c91f6199', 'access_key': '83bf965b-dfec-42a0-b451-404a06e987dc'},   # MA House #5
# ~ {'id': '5740035f-bdeb-4465-aad4-58bd4f9ede3b', 'access_key': 'f9dde7b7-5c4b-47ca-8200-115844dc70ea'},   # MA House #11
# ~ {'id': 'df62923b-6b7d-43a9-abe6-3b66e31a9831', 'access_key': '2886e32f-6203-445f-a781-9690839aba37'},   # MA House #13
# ~ {'id': 'ee0c4938-d8f9-43b4-a09d-6695b1f49406', 'access_key': 'ff3ac093-8858-4502-9a44-bd16048870c1'},   # MA House #15
# ~ {'id': '888d6224-05ee-48a9-94a0-5318ff6035b5', 'access_key': '0bce3478-94da-4acb-8287-bf02cedee4a6'},   # MA House #16
# ~ {'id': '7032ba9f-1c7f-4944-9414-bc414f5d8646', 'access_key': 'f93dbd08-6344-4861-b7f5-2e1c979eb285'},   # MA House #17
# ~ {'id': '1a734074-a88f-4d69-8886-bc21c5e874e3', 'access_key': 'c59713e6-f169-4289-84f5-3447ec608957'},   # MA House #18
# ~ ]
# ~ v2_users = [
# ~ {'id': '5eb9304115627e90948dd297', 'user_id': '22465020-da7e-4456-9ad0-b539904ae438', 'name': 'Test 1'},
# ~ {'id': '5eb9306f15627e90948dd298', 'user_id': '0f81d73e-37df-4269-a00e-848bb8411794', 'name': 'Test 2'},
# ~ {'id': '5eb930a915627e90948dd299', 'user_id': 'a52ac50e-4fd0-498f-96e5-534c9ee4e7e1', 'name': 'Test 3'},
# ~ {'id': '5eb930c915627e90948dd29a', 'user_id': '4fdb4bab-9b26-4d41-8370-d8e92187a527', 'name': 'Test 4'},
# ~ {'id': '5eb9311315627e90948dd29b', 'user_id': '8ec41708-64c8-4a3c-83e4-29479ed4c9b1', 'name': 'Test 5'},
# ~ {'id': '5eb9312815627e90948dd29c', 'user_id': '4fe17cfb-976e-4f4c-9e84-4a96b34f85dd', 'name': 'Test 6'},
# ~ {'id': '5eb9314215627e90948dd29d', 'user_id': 'ef71753a-9e4e-46b6-b037-899529df1264', 'name': 'Test 7'},
# ~ {'id': '5eb9316715627e90948dd29e', 'user_id': '8c2c36a0-b2bf-483e-8994-5eea7868c525', 'name': 'Test 8'},
# ~ {'id': '5eb9318115627e90948dd29f', 'user_id': '7eeba885-ac08-4ccb-98e6-d7eb7a8a33db', 'name': 'Test 9'},
# ~ {'id': '5eb9319b15627e90948dd2a0', 'user_id': 'ea573aa2-e301-4252-ae01-7390cc002153', 'name': 'Test 10'},
# ~ ]

#
#   PRODUCTION ENVIRONMENT
#
v1_users = [
    {
        "id": "b96c8169-0fb4-4c04-aef5-7464ccc3b539",
        "access_key": "6247c9e3-d5ce-4b64-a0ea-73be088fed71",
    },  # Test #1
    {
        "id": "5033f696-a930-44ab-8920-a64b41097e38",
        "access_key": "ddf9ce4a-9754-47d7-b446-58c0c1ad9bcf",
    },  # Test #2
    {
        "id": "6730ac24-3b15-4795-95de-ca3e7b0411a6",
        "access_key": "80820ac5-532e-439f-8cf1-978e8523e55c",
    },  # Test #3
    {
        "id": "7e557a0d-cf59-4eb8-bef8-9292518536b2",
        "access_key": "c7bd46a1-a7d9-4d84-81f5-494b5862c386",
    },  # Test #4
    {
        "id": "55408b19-1db7-45a1-a0d4-535f2d72a3bc",
        "access_key": "24d09053-cfeb-40db-a2d8-efed685578e6",
    },  # Test #5
    {
        "id": "e2a3d58f-9d7a-442b-95c6-dd0cb9553ca2",
        "access_key": "62fa6a72-574e-4c7a-85b0-67e5320ed4b4",
    },  # Test #6
    {
        "id": "023cee57-6b0b-486e-8579-eab56be39a0c",
        "access_key": "1dda4a96-9702-44de-a559-5a42ff4aedc3",
    },  # Test #7
    {
        "id": "31fb5b00-9e34-4845-bd1b-5108270294ab",
        "access_key": "50a116be-936d-4dce-b708-f977eeeb2f3f",
    },  # Test #8
    {
        "id": "6eee70cd-d3aa-4215-9b00-c7bee762d495",
        "access_key": "a2760061-4caa-4c0b-8872-9432194c4ec2",
    },  # Test #9
    {
        "id": "c3cf4760-efb8-4307-b7da-9b7a499f6996",
        "access_key": "dcac9455-7679-4968-87ca-87caf44cad1c",
    },  # Test #10
]

v2_users = [
    {
        "id": "5ebeec8a670fe9936f01e2d6",
        "user_id": "bf297ea5-c338-4d51-bd93-086f6a115233",
        "name": "Test 1",
    },
    {
        "id": "5ebeed4f670fe9936f01e2d7",
        "user_id": "5017076b-ab9e-49bb-91cd-4f8d627a1b50",
        "name": "Test 2",
    },
    {
        "id": "5ebeed6b670fe9936f01e2d8",
        "user_id": "a7e2ae4a-fb36-485d-93f8-381ed0ea1f61",
        "name": "Test 3",
    },
    {
        "id": "5ebeed88670fe9936f01e2d9",
        "user_id": "55dcc86e-99fe-409f-aaa4-7d27eb222071",
        "name": "Test 4",
    },
    {
        "id": "5ebeeda6670fe9936f01e2da",
        "user_id": "60929478-a1c3-47c9-9d0c-1e37b3677426",
        "name": "Test 5",
    },
    {
        "id": "5ebeedba670fe9936f01e2db",
        "user_id": "04e0b145-8138-4401-913c-2d6c3e403c03",
        "name": "Test 6",
    },
    {
        "id": "5ebeedd4670fe9936f01e2dc",
        "user_id": "a71ca959-e258-49ab-bbee-fae789a8da08",
        "name": "Test 7",
    },
    {
        "id": "5ebeedeb670fe9936f01e2dd",
        "user_id": "60ef39f0-4fe7-47e0-8556-e34cce8cfa81",
        "name": "Test 8",
    },
    {
        "id": "5ebeee02670fe9936f01e2de",
        "user_id": "f2541b83-8233-4b3b-a6f8-c220eea710b5",
        "name": "Test 9",
    },
    {
        "id": "5ebeee1a670fe9936f01e2df",
        "user_id": "2243189c-738c-4873-b9ab-44749f9161a8",
        "name": "Test 10",
    },
]
